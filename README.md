
# Polynomials

#### Basics

```
>>> from factoring import Polynomial
>>> p = Polynomial([1, 2, 3, 1])
>>> print(p)
x**3+2*x**2+3*x+1
```
##### Graphing
Uses Python's Turtle graphics to draw a graph. Waits 15 seconds after drawing before clearning the picture.

Polynomial.**graph**(*lower*=0, *upper*=10, *distance*=0.1)

| Parameters        | Type          |
| -------------     |:------------- | 
| *lower*           | Number        |
| *upper*           | Number        |
| *distance*        | Number        |

Graph the polynomial from *lower* to *upper*, with *distance* between every point.

  ```
   >>> p = Polynomial([1, 2, 1])
   >>> p.graph(lower=-10)
   graphed x**2+2*x+1 from -10 to 10
  ```
![alt text][logo]

[logo]: https://github.com/isthisnagee/Polynomials/blob/master/images/graph.png "Graph of x^2+2x+1"

##### Multiplication
Multiplies a polynomial to another polynomial or to a number, returning a Polynomial object
```
>>> p = Polynomial([1, 1])
>>> q = Polynomial([1, 1])
>>> x = p*q
>>> x
Polynomial([1, 2, 1])
>>> w = x * 3
>>> w
Polynomial([3, 6, 3])
```

##### Integration
Integrates a polynomial, returning a Polynomial object.
```
>>> p = Polynomial([1, 2, 1])
>>> w = p.integrate()
>>> w
Polynomial([Fraction(1, 3), Fraction(1, 1), Fraction(1, 1), 0])
>>> print(w)
1/3*x**3+x**2+x
```

##### Differentiation
Differentiates a polynomial, returning a Polynomial object.
```
>>> p = Polynomial([1, 2, 1])
>>> print(p)
x**2+2*x+1
>>> x = p.derivate()
>>> print(x)
2*x+2
```
##### Division
Coming soon

Divides two polynomials, returning a Polynomial object, if possible.

##### Factoring
Coming soon

Factors a Polynomial, returning a list of lists of coefficients

By Nagee Elghassein

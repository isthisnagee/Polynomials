import re
from fractions import Fraction
from turtle import Turtle
from time import sleep
from random import choice
# Helper Functions #
# ============================================ #


def gather_lists(L):
    ''' (list-of-lists) -> list

    Concatenate all the sublists of L and return the result.

    >>> gather_lists([[1, 2], [3, 4, 5]])
    [1, 2, 3, 4, 5]
    >>> gather_lists([[6, 7], [8], [9, 10, 11]])
    [6, 7, 8, 9, 10, 11]
    '''
    new_list = []
    for l in L:
        new_list += l
    return new_list


def constant_type(x):
    '''(list of parts) -> int

    Return the leading integer of the string, if it exists, else 1.

    >>> constant_type('-2*x')
    -2
    >>> constant_type('x')
    1
    '''
    if x.isdigit():
        return int(x)
    elif x[1:].isdigit() and x[0] == '-':
        return int(x)
    elif x[0] == '-' and x[1].isdigit():
        variable = re.search('x', x)
        return int(x[0:variable.start()-1])
    elif x[0] == '-' and x[1].isalpha():
        return -1
    elif x[0].isalpha():
        return 1
    else:
        variable = re.search('x', x)
        return int(x[0:variable.start()-1])
# ============================================ #
# End Helper Functions #


class Polynomial:
    '''
    A class for generic polynomials
    '''

    def __init__(self, polynomial):
        ''' (list of int) -> Nonetype
        Initialize polynomial

        >>> p = Polynomial([1, 2, -3])
        >>> p.polynomial
        [1, 2, -3]
        >>> p.coefficient
        1
        >>> p.degree
        2
        '''
        self.polynomial = polynomial
        # the first index represents the coefficient, the second the degree.
        self._polynomial = [(polynomial[x], len(polynomial) - x - 1) for x in
                            range(len(polynomial))]

        self.coefficient = polynomial[0]
        self.factored = None
        self.degree = len(polynomial) - 1  # get self.degree
        self.constants = polynomial[0]
        # self.factor()

    def __str__(self, variable='x'):
        s = ''
        for item in self._polynomial:
            # case 0
            if item[0] == 0:
                s += ''
            # case nx^k, n != 0, 1
            elif item[1] != 1 and item[1] != 0:
                if item[0] == 1:
                    s += '+{}**{}'.format(variable, item[1])
                elif item[0] == -1:
                    s += '-{}**{}'.format(variable, item[1])
                elif item[0] < 0 and item[0] != -1:
                    s += '{}*{}**{}'.format(item[0], variable, item[1])
                else:
                    s += '+{}*{}**{}'.format(item[0], variable, item[1])
            # case nk, n != 0
            elif item[1] == 1:
                if item[0] == 1:
                    s += '+{}'.format(variable)
                elif item[0] == -1:
                    s += '-{}'.format(variable)
                elif item[0] < 0 and item[0] != -1:
                    s += '{}*{}'.format(item[0], variable)
                else:
                    s += '+{}*{}'.format(item[0], variable)
            # case n, n != 0
            else:
                if item[0] < 0 and item[0] != -1:
                    s += str(item[0])
                else:
                    s += '+{}'.format(item[0])
        if s[0] == '+':
            s = s[1:]
        return s

    def __repr__(self):
        ''' (Polynomial) -> str
        Return a string representation of Polynomial that can be evaluated
        '''
        return 'Polynomial({})'.format(self.polynomial)

    def __eq_(self, other):
        ''' (Polynomial, Polynomial) -> bool

        Return if self is equivalent to other

        >>> p = Polynomial([1, 2, 3])
        >>> q = Polynomial([1, 3, 4])
        >>> p == q
        False
        '''

        return isinstance(other,
                          Polynomial) and (other.polynomial == self.polynomial)

    def __mul__(self, other):
        ''' (Polynomial, Polynomial) -> Polynomial

        Multiply self and other to get a object for another Polynomial Instance

        >>> p = Polynomial([1, 2, 1])
        >>> q = Polynomial([1, 1])
        >>> r = p * q
        >>> print(r)
        x**3+3*x**2+4*x+1
        '''
        if isinstance(other, int) or isinstance(other, float):
            multiplied = [x[0] * other for x in self._polynomial]
            return Polynomial(multiplied)

        elif isinstance(other, Polynomial):
            multiplied = []
            added = []
            for x in self._polynomial:
                mult = [(x[0]*y[0], x[1]+y[1]) for y in other._polynomial]
                multiplied.append(mult)
            j = len(multiplied)
            i = len(multiplied) - 1
            added.append(multiplied[0][0])
            for k in range(int((len(multiplied) + 1) / 2)):
                for r in range(1, len(multiplied[k])):
                    x = (multiplied[k][r][0]+multiplied[j-i][r-1][0],
                         multiplied[k][r][1])
                    added.append(x)
            added.append(multiplied[-1][-1])
            view_added = [x[0] for x in added]
            return Polynomial(view_added)

    def __truediv__(self, other):
        ''' (Polynomial, Polynomial) -> Polynomial

        Return a Polynomial object if divisible, else
            return an error

        EX
        '''
        if len(self.polynomial) >= len(other.polynomial):
            pass
            # implement thing here

    def integrate(self):
        ''' (Polynomial) -> Polynomial

        Return the integral of the Polynomial
        '''
        x = [(Fraction(x[0], x[1]+1), x[1])for x in self._polynomial]
        return Polynomial([w[0] for w in x] + [0])

    def derivate(self):
        x = [(x[0]*(x[1]), x[1] - 1)for x in self._polynomial]
        return Polynomial([w[0] for w in x][0:-1])

    def graph(self, x_lower=0, x_upper=10, space=0.1, y_lower=10, y_upper=-10):
        t = Turtle()
        # make axis
        t.up()
        t.goto(0, 0)
        t.down()
        # graph x axis
        t.goto(0, x_upper * 10)
        t.goto(0, x_lower * 10 if x_lower < 0 else -1 * x_upper * 10)
        t.up()
        # graph y axis
        t.goto(y_upper*10, 0)
        t.down()
        t.goto(y_lower*10, 0)
        t.goto(0, 0)
        t.down()
        colors = ['blue', 'hotpink', 'red']
        # pick a random color for the graph
        t.color(choice(colors))
        t.hideturtle()
        # start drawing
        l = x_lower
        if l < 0:
            t.up()
            t.goto(l*x_upper, sum([y_upper*x[0]*l**x[1] for x in self._polynomial]))
            t.down()
        while l <= x_upper and sum([y_upper*x[0]*l**x[1] for x in self._polynomial]) < y_upper*3:
            if l == 0:
                t.up()
                t.goto(0, sum([y_upper*x[0]*l**x[1] for x in self._polynomial]))
                t.down()
            t.goto(l*x_upper, sum([y_upper*x[0]*l**x[1] for x in self._polynomial]))
            l += space
        t.up()
        t.goto(0,0)
        t.color('black')
        print('graphed {} from {} to {}'.format(str(self), x_lower, x_upper))


    # def get_degree(self):
    #     ''' (Polynomial) -> NoneType
    #     Get the degree of the polynomial
    #
    #     >>> p = Polynomial('4*x**3-16*x**2')
    #     >>> p.degree
    #     3
    #     '''
    #     location = re.search('\*\*', self.polynomial)
    #     self.degree = int(self.polynomial[location.end()]) \
    #                   if str(location) != 'None' else 1
    #     if self.degree == 1 and not 'x' in self.polynomial:
    #         self.degree = 0
    #
    # def get_parts(self):
    #     ''' (Polynomial) -> NoneType
    #     Get the parts of the polynomial
    #
    #     >>> p = Polynomial('-3*x**3-3*x+3')
    #     >>> p.parts
    #     ['-3*x**3', '-3*x', '3']
    #     '''
    #     split_minus = self.polynomial.split('-')
    #     split_minus = [split_minus[0]] + ['-'+x for x in split_minus[1:]]
    #     split_minus = [x for x in split_minus if x != '']
    #     split = gather_lists([x.split('+') for x in split_minus])
    #     self.parts = split
    #
    # def get_constant(self):
    #     ''' (Polynomial) -> NoneType
    #     Get the constant of the polynomial, 0 if they are missing
    #
    #     >>> p = Polynomial('4*x**3-3*x**2+4')
    #     >>> p.constants
    #     [4, -3, 0, 4]
    #     '''
    #     constants = []
    #     d = self.degree
    #     for i in range(d, -1, -1):
    #         print(i)
    #         if i > 1:
    #             if '**{}'.format(i) in self.parts[d-i]:
    #                 print('added: '+str(constant_type(self.parts[d-i])))
    #                 constants.append(constant_type(self.parts[d-i]))
    #             else:
    #                 print('added 0')
    #                 constants.append(0)
    #         else:
    #             constants.append(constant_type(self.parts[d-i]))
    #
    #
    #     # for x in self.parts:
    #     #     if i >= 0:
    #     #         if i > 1:
    #     #             if not '**{}'.format(str(i)) in x:
    #     #                 constants.append(0)
    #     #                 constants.append(constant_type(x))
    #     #                 i -= 2
    #     #             else:
    #     #                 constants.append(constant_type(x))
    #     #                 i -= 1
    #     #         elif i == 1:
    #     #             if not 'x' in x:
    #     #                 constants.append(0)
    #     #                 constants.append(constant_type(x))
    #     #                 i -= 2
    #     #             else:
    #     #                 constants.append(constant_type(x))
    #     #                 i-=1
    #     #         else:
    #     #             constants.append(constant_type(x))
    #     #             i-=1
    #     self.constants = constants

    # def factor(self):
    #     raise NotImplementedError('Subclass Needed.')

if __name__ == '__main__':
    import doctest
    doctest.testmod()

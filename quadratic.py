from factoring import Polynomial

class Quadratic(Polynomial):

    def __init__(self, quadratic):
        ''' (Polynomial, str) -> NoneType
        Precondition: No whitespace in quadratic
        Initialize the quadratic.

        >>> q = Quadratic('x**2-1')
        >>> q.constants
        [1, 0, -1]
        >>> q.factored
        '(x+1.0)(x-1.0)'
        '''
        Polynomial.__init__(self, quadratic)

    def __str__(self):
        ''' (Quadratic) -> str
        Return a representation of the quadratic

        >>>
        '''
        return """
            Equation: {}
            Factored: {}
        """.format(self.polynomial, self.factored)

    def factor(self):
        ''' (Polynomial) -> NoneType
        Get the factored form of the polynomial

        >>> q = Quadratic('x**2-1')
        >>> q.factored
        '(x+1.0)(x-1.0)'
        '''
        a, b, c = self.constants[0], self.constants[1], self.constants[2]
        formula_positive = (-b + (b**2 - (4 * a * c))**.5)/ (2 * a)
        formula_negative = (-b - (b**2 - (4 * a * c))**.5)/ (2 * a)
        x_1 = '(x+{})'.format(formula_positive) if (not '-' in str(formula_positive)) else \
                    '(x-{})'.format(abs(formula_positive))
        x_2 = '(x-{})'.format(abs(formula_negative)) if '-' in str(formula_negative) else \
                    '(x+{})'.format(formula_negative)
        self.factored = x_1 + x_2

if __name__ == '__main__':
    import doctest
    doctest.testmod()
